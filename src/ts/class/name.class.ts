export interface IContact {
    firstName:  string;
    lastName: string;
    email?: string;
    company?: string;
    phone?: string;
    created_date?: string;
}

export class Contact {

    data: IContact;

    constructor(contact: IContact = null) {
        this.init(contact);
    };

    init(contact: IContact = null) {
        if(contact) {
            this.data = { ...contact };
        }
    };

    hi() {
        
        console.log(`Hi ${this.data ? this.data.firstName : ''}`)
    };

    getContacts() {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:3000',
            dataType: 'json',
            success: res => console.log(res),
          });
    }

};