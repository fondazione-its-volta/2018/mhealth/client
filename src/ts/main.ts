import '../scss/main.scss';
import { Contact } from './class/name.class';

$(document).ready(
    () => {
        let instance = new Contact({firstName: 'Daniel', lastName: 'Zotti'});
        instance.hi();
        $('.jumbotron').css('margin-top',100);
        instance.getContacts();
    }
)