# mHealth client

## Plugin necessari per VSCode

Installare `Live Server` per eseguire il client

## Inizializzazione progetto

Eseguire `npm install`

## Esecuzione del server di sviluppo

Eseguire `npm run dev`, pulsante destro su `index.html` e cliccare su `Open with Live Server`

## Build

Eseguire `npm run build`. I file verranno compilati e salvati nella folder `dist/`